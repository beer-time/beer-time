package com.rol_dyn.beerapp;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BreathalyserTest {

    @Test
    public void getBAC() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, -1);
        List<Beer> input = new ArrayList<Beer>() {
            {
                add(new Beer(cal.getTime(), 5, 500));
            }
        };
        float output;
        float delta = 0.01f;
        float expected = 0.2f;

        output = Breathalyser.getBAC(input, true, 80);
        assertEquals(expected, output, delta);
    }

    @Test
    public void getGramsOfAlcoholConsumed() {
        List<Beer> input = new ArrayList<Beer>() {
            {
                add(new Beer(Calendar.getInstance().getTime(), 5, 500));
                add(new Beer(Calendar.getInstance().getTime(), 5, 500));
            }
        };
        float output;
        float expected = 40f;
        float delta = 1f;

        output = Breathalyser.getGramsOfAlcoholConsumed(input);
        assertEquals(expected, output, delta);
    }

    @Test
    public void multiplyWeightByGenderConstant() {
        float input = 80;
        boolean male = true;
        float output;
        float expected = 54400f;
        float delta = 1f;
        output = Breathalyser.multiplyWeightByGenderConstant(input, male);
        assertEquals(expected, output, delta);
    }

    @Test
    public void getRawAlcoholGramsInBody() {
        float multipledWeightInput = 54400f;
        float alcoholInGramsInput = 40f;
        float output;
        float delta = 0.00001f;
        float expected = 0.000735f;
        output = Breathalyser.getRawAlcoholGramsInBody(multipledWeightInput, alcoholInGramsInput);
        assertEquals(expected, output, delta);
    }

    @Test
    public void getTotalAlcoholInPerMiles() {
        float input = 0.000735f;
        float output;
        float delta = 0.01f;
        float expected = 0.735f;
        output = Breathalyser.getTotalAlcoholInPerMiles(input);
        assertEquals(expected, output, delta);
    }

    @Test
    public void getAlcoholLevelBasedOnTimeElapsed() {
        float perMileInput = 0.5f;
        float hoursElapsed = 2;
        float output;
        float delta = 0.01f;
        float expected = 0.2f;

        output = Breathalyser.getAlcoholLevelBasedOnTimeElapsed(perMileInput, hoursElapsed);
        assertEquals(expected, output, delta);
    }

    @Test
    public void getTimeDifference() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, -2);
        Beer input = new Beer(cal.getTime(), 5, 500);
        float output;
        float delta = 0.01f;
        float expected = 2;
        output = Breathalyser.getTimeDifference(input);
        assertEquals(expected, output, delta);
    }
}