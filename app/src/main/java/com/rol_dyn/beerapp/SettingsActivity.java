package com.rol_dyn.beerapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

public class SettingsActivity extends AppCompatActivity {

    private TextInputLayout weightInput;
    private TextInputLayout promileInput;
    private RadioGroup genderGroup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);

        genderGroup = findViewById(R.id.genderRadio);
        weightInput = findViewById(R.id.weightLayout_input);
        promileInput = findViewById(R.id.promiles_input);
    }

    public void submit(View view) {
        if (isInputValid()) {
            saveToSharedPreferences();
            setAsLaunched();
            switchToMenuActivity();
        }
    }

    private void setAsLaunched() {
        SharedPreferences sharedPreferences = getSharedPreferences("userDetails", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("firstStart", false);
        editor.apply();
    }


    public boolean isInputValid() {
        boolean correct = true;
        if (weightInput.getEditText().getText().toString().trim().length() == 0) {
            weightInput.setError(getText(R.string.weightIsEmpty));
            correct = false;
        }
        if (promileInput.getEditText().getText().toString().trim().length() == 0) {
            promileInput.setError(getText(R.string.promilesIsEmpty));
            correct = false;
        }
        if(genderGroup.getCheckedRadioButtonId() == -1){
            ((RadioButton)genderGroup.getChildAt(0)).setError(getString(R.string.selectGender));
            correct = false;
        }

        return correct;
    }

    public void saveToSharedPreferences() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("userDetails", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat("weight", Float.parseFloat(weightInput.getEditText().getText().toString()));
        editor.putFloat("perMilTarget", Float.parseFloat(promileInput.getEditText().getText().toString()));
        if(genderGroup.getCheckedRadioButtonId() == R.id.radioButton_male){
            editor.putBoolean("isMale", true);
        }else {
            editor.putBoolean("isMale", false);
        }

        editor.apply();
    }

    private void switchToMenuActivity() {
        Intent intent = new Intent(this, MenuActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(intent);

    }
}
