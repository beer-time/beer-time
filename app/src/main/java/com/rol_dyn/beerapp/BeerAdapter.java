package com.rol_dyn.beerapp;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class BeerAdapter extends RecyclerView.Adapter<BeerAdapter.BeerHolder> {
    private List<Beer> beers = new ArrayList<>();

    @NonNull
    @Override
    public BeerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.beer_item, parent, false);
        return new BeerHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BeerHolder holder, int position) {
        Beer currentBeer = beers.get(position);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        holder.textViewDate.setText(sdf.format(currentBeer.getDate()));
        holder.textViewCapacity.setText(String.valueOf(currentBeer.getCapacity()));
        holder.textViewPercents.setText(String.valueOf(currentBeer.getPercents()));
        holder.textViewName.setText(currentBeer.getName());
    }

    @Override
    public int getItemCount() {
        return beers.size();
    }

    public Beer getItemAt(int position) {
        return beers.get(position);
    }

    public Beer getLastItem() {
        return beers.get(beers.size() - 1);
    }

    public void updateItemAt(int position, Beer updatedBeer) {
        Beer beerToUpdate = beers.get(position);
        beerToUpdate.setCapacity(updatedBeer.getCapacity());
        beerToUpdate.setName(updatedBeer.getName());
        beerToUpdate.setPercents(updatedBeer.getPercents());
        notifyDataSetChanged();
    }

    public void removeItemAt(int position) {
        beers.remove(position);
        notifyDataSetChanged();
    }

    public void setBeers(List<Beer> beers) {
        this.beers = beers;
        notifyDataSetChanged();
    }


    class BeerHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        private TextView textViewDate;
        private TextView textViewPercents;
        private TextView textViewCapacity;
        private TextView textViewName;

        public BeerHolder(@NonNull View itemView) {
            super(itemView);
            textViewDate = itemView.findViewById(R.id.text_view_date);
            textViewPercents = itemView.findViewById(R.id.text_view_percents);
            textViewCapacity = itemView.findViewById(R.id.text_view_capacity);
            textViewName = itemView.findViewById(R.id.text_view_name);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

            contextMenu.add(getAdapterPosition(), 1, 0, view.getResources().getString(R.string.edit));
            contextMenu.add(getAdapterPosition(), 2, 0, view.getResources().getString(R.string.delete_text));
        }
    }

}
