package com.rol_dyn.beerapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.PropertyValuesHolder;
import android.animation.TimeAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;

import androidx.core.math.MathUtils;

import java.util.Random;

public class BeerFillable extends View {
    //Default variables
    private static final float DEFAULT_PERMILE_FILLED = 0.1f;
    private static final float DEFAULT_MAX_PERMILE = 1.0f;
    private static final int DESK_COLOR = Color.parseColor("#452608");
    private static final int GLASS_COLOR = Color.parseColor("#459cf2ff");
    private static final int GLASS_RELFECTION = Color.parseColor("#65b7f7ff");
    private static final int BEER_TOP = Color.parseColor("#bc7012");
    private static final int BEER_BOTTOM = Color.parseColor("#efd002");
    private static final float BEER_HEIGHT = 170f;
    private static final float BEER_WIDTH = 120f;
    private static float density = 160f;

    private static final int BASE_SPEED_DP_PER_S = 3;
    private static final int COUNT = 20;
    private static final int SEED = 777;

    private static final float SCALE_MIN_PART = 0.45f;
    private static final float SCALE_RANDOM_PART = 0.55f;
    private static final float ALPHA_SCALE_PART = 0.5f;
    private static final float ALPHA_RANDOM_PART = 0.5f;

    Paint paint;
    Paint beerGradientPaint;
    Paint fontPaint;
    Paint bubblePaint;
    private Bitmap background;
    private Shader beerShader;

    private float perMileFilled;
    private float maxPermile;


    private Matrix translateMatrix = new Matrix();
    private Matrix skewMatrix = new Matrix();

    private boolean shouldShowGlass = false;
    private boolean shouldShowText = false;
    private boolean shouldShowBubles = false;

    private float beerLevel;
    private int textOpacity = 0;
    private float glassXposition = 0;
    private float beerYskew = .03f;
    private float baseSize;

    private static class Bubble{
        float x;
        float y;
        float scale;
        float alpha;
        float speed;
    }
    private final Bubble[] bubbles = new Bubble[COUNT];
    private final Random rand = new Random(SEED);

    private void initializeBubble(Bubble bubble, float width, float height){

        bubble.scale = SCALE_MIN_PART + SCALE_RANDOM_PART * rand.nextFloat();
        bubble.x = width* rand.nextFloat() + 10f*density;
        bubble.y = height;
        bubble.y += bubble.scale * baseSize;
        bubble.y += height * rand.nextFloat()/4f;

        bubble.alpha = ALPHA_SCALE_PART * bubble.scale + ALPHA_RANDOM_PART * rand.nextFloat();
        bubble.speed = (BASE_SPEED_DP_PER_S*density) * bubble.alpha * bubble.scale;
    }

    public BeerFillable(Context context) {
        super(context);
    }

    public BeerFillable(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BeerFillable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        final TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.BeerFillable, defStyleAttr, 0);
        init(attributes);
        attributes.recycle();
        paint = new Paint();
        density = getResources().getDisplayMetrics().density;
        beerGradientPaint = new Paint();
        beerGradientPaint.setStyle(Paint.Style.FILL);


        bubblePaint = new Paint();
        bubblePaint.setColor(Color.WHITE);

        fontPaint = new Paint();
        fontPaint.setTextSize(22 * getResources().getDisplayMetrics().scaledDensity);
        fontPaint.setTextAlign(Paint.Align.CENTER);
        fontPaint.setAntiAlias(true);
        fontPaint.setFakeBoldText(false);

        paint.setStyle(Paint.Style.FILL);

    }

    private void animateText(){
        shouldShowText = true;
        PropertyValuesHolder textAlpha = PropertyValuesHolder.ofInt("opacity", 0, 255);
        ValueAnimator animator = new ValueAnimator();
        animator.setDuration(1000);
        animator.setValues(textAlpha);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                textOpacity = (int) animation.getAnimatedValue("opacity");
                postInvalidate();
            }
        });
        animator.start();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        beerShader =  new LinearGradient(0, ((5/6f)*getHeight()-(0.7f*BEER_HEIGHT*density)) , 0,  ((5/6f)*getHeight()), BEER_TOP, BEER_BOTTOM, Shader.TileMode.CLAMP);

        for(int i = 0; i < bubbles.length; i++ ){
            final Bubble bubble = new Bubble();
            initializeBubble(bubble, (BEER_WIDTH-20f)*density, BEER_HEIGHT*density);
            bubbles[i] = bubble;
        }
        animateGlass();
    }

    private void init(TypedArray attributes) {
        perMileFilled = attributes.getFloat(R.styleable.BeerFillable_currentPerMile, DEFAULT_PERMILE_FILLED);
        maxPermile = attributes.getFloat(R.styleable.BeerFillable_maxPerMile, DEFAULT_MAX_PERMILE);
        background = BitmapFactory.decodeResource(getResources(), R.drawable.background);
        baseSize = 10f;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float percent = MathUtils.clamp(perMileFilled / maxPermile, 0f, 1f);
        beerLevel = (1f-percent)*BEER_HEIGHT*density;
        beerGradientPaint.setShader(beerShader);

        drawBackground(canvas);

        if(shouldShowGlass)
        {
            translateMatrix.setTranslate(glassXposition-((BEER_WIDTH/2)*density), ((9/10f)*getHeight()-(BEER_HEIGHT*density)));
            skewMatrix.setSkew(0, beerYskew, (BEER_WIDTH/2f)*density, 0);
            Path glass = getGlass();
            Path glassReflection = getGlassReflection();
            glass.transform(translateMatrix);
            Path beer = getBeer(percent);
            Path foam = getBeerFoam(percent);
            beer.transform(skewMatrix);
            beer.transform(translateMatrix);
            foam.transform(skewMatrix);
            foam.transform(translateMatrix);
            paint.setColor(GLASS_COLOR);
            canvas.drawPath(glass, paint);
            canvas.save();

            canvas.clipPath(glass, Region.Op.INTERSECT);
            canvas.drawPath(beer, beerGradientPaint);
            paint.setColor(Color.WHITE);
            canvas.drawPath(foam, paint);
            canvas.restore();
            glassReflection.transform(translateMatrix);

            paint.setColor(GLASS_RELFECTION);
            canvas.drawPath(glassReflection, paint);
        }
        if(shouldShowBubles && percent > 0.1f){
            for(final Bubble bubble : bubbles){
                final float bubbleSize = bubble.scale * baseSize;
                if(bubble.y + bubbleSize < 0 || bubble.y - bubbleSize > BEER_HEIGHT*density ){
                    continue;
                }
                Path circle = new Path();
                circle.addCircle(bubble.x, bubble.y, bubble.scale*baseSize, Path.Direction.CW);
                circle.transform(translateMatrix);
                bubblePaint.setAlpha(Math.round(255*bubble.alpha));
                canvas.drawPath(circle, bubblePaint);
            }
        }
        if(shouldShowText){
            fontPaint.setAlpha(textOpacity);
            canvas.drawText(String.valueOf(perMileFilled) + '\u2030', getWidth()/2f , ((9/10f)*getHeight()-(BEER_HEIGHT/2*density)), fontPaint);
        }

        drawTable(canvas);

    }

    private void updateState(float deltaTime){
        final float deltaSeconds = deltaTime/1000f;
        final float viewHeight = BEER_HEIGHT*density;

        for(final Bubble bubble : bubbles){
            bubble.y -= bubble.speed *deltaSeconds;

            final float size = bubble.scale * baseSize;
            if(bubble.y < beerLevel + size){
                initializeBubble(bubble,(BEER_WIDTH-20f)*density, viewHeight);
            }
        }
    }

    private void animateBubbles(){
        shouldShowBubles = true;
        TimeAnimator timeAnimator = new TimeAnimator();
        timeAnimator.setTimeListener(new TimeAnimator.TimeListener() {
            @Override
            public void onTimeUpdate(TimeAnimator animation, long totalTime, long deltaTime) {
                if(!isLaidOut()){
                    return;
                }
                updateState(deltaTime);
                invalidate();
            }
        });
        timeAnimator.start();
    }

    private Path getBeerFoam(float percent){
        float x = 10*(1-percent)*density;
        float xD = x*(BEER_HEIGHT/10f);
        Path path = new Path();
        path.moveTo(0,xD);
        path.lineTo(0,xD+(1.2f*percent*density*10));
        path.lineTo(BEER_WIDTH*density, xD+(1.2f*percent*density*10));
        path.lineTo(BEER_WIDTH*density, xD);
        path.close();
        return path;
    }

    public void animateFluid(){
        PropertyValuesHolder fluidSkew = PropertyValuesHolder.ofFloat("skew", .02f, -.02f);
        ValueAnimator animator = new ValueAnimator();
        animator.setDuration(10000);
        animator.setValues(fluidSkew);
        animator.setInterpolator(new CycleInterpolator(5));
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                beerYskew = (float) animation.getAnimatedValue("skew");
                postInvalidate();
                animation.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        beerYskew = 0;
                    }
                });
            }
        });
        animator.start();
    }

    public void animateGlass(){
        shouldShowGlass = true;

        PropertyValuesHolder glassPosition = PropertyValuesHolder.ofFloat("position", 0, getWidth() / 2f);
        ValueAnimator animator = new ValueAnimator();
        animator.setValues(glassPosition);
        animator.setDuration(500);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                glassXposition = (float) animation.getAnimatedValue("position");
                postInvalidate();
                animation.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        animateBubbles();
                        animateFluid();
                        animateText();
                    }
                });
            }
        });
        animator.start();
    }
    private Path getBeer(float percent){
        Path path = new Path();
        path.moveTo(10f*density, BEER_HEIGHT*density);
        path.lineTo((BEER_WIDTH-10f)*density, BEER_HEIGHT*density);
        float percentInv = 1f-percent;
        float x = 10*percentInv*density;
        path.lineTo((BEER_WIDTH-10f)*density+(10*density), x*(BEER_HEIGHT/10f) );
        path.lineTo(x, x*(BEER_HEIGHT/10f));
        path.close();
        return path;
    }

    private Path getGlass(){
        Path path = new Path();
        path.moveTo(0,0);
        path.lineTo(BEER_WIDTH*density, 0);
        path.lineTo((BEER_WIDTH-10f)*density, BEER_HEIGHT*density);
        path.lineTo(10f*density, BEER_HEIGHT*density);
        path.close();
        return path;
    }
    private Path getGlassReflection(){
        Path path = new Path();
        path.moveTo((BEER_WIDTH/9)*density, 0f);
        path.lineTo((BEER_WIDTH/9)*2*density, 0f);
        path.lineTo((BEER_WIDTH/9)*3*density, BEER_HEIGHT*density);
        path.lineTo((BEER_WIDTH/9)*2*density, BEER_HEIGHT*density);
        path.close();
        return path;
    }


    private void drawBackground(Canvas canvas){
        canvas.drawBitmap(background,0,0,null);
    }
    private void drawTable(Canvas canvas){
        RectF rectF = new RectF(0f, (9/10f)*getHeight(), getWidth(), getHeight());
        paint.setColor(DESK_COLOR);
        paint.setAntiAlias(true);
        canvas.drawRect(rectF, paint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);

    }

    public void setPerMileFilled(float currentPermiles) {
        this.perMileFilled = currentPermiles;
    }

    public void setMaxPermile(float maxPermiles) {
        this.maxPermile = maxPermiles;
    }

    public void redraw() {
        this.postInvalidate();
    }
}
