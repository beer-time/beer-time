package com.rol_dyn.beerapp;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;


@Database(entities = {Beer.class}, version = 2, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class BeerAppDatabase extends RoomDatabase {
    private static BeerAppDatabase instance;

    public static synchronized BeerAppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), BeerAppDatabase.class, "beerApp_database").fallbackToDestructiveMigration().allowMainThreadQueries().build();
        }
        return instance;
    }

    public abstract BeerDao beerDao();
}
