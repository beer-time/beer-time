package com.rol_dyn.beerapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.Date;
import java.util.List;

@Dao
public interface BeerDao {
    @Query("SELECT * FROM beer ORDER BY id DESC")
    LiveData<List<Beer>> getAll();

    @Query("SELECT * FROM beer ORDER BY id DESC LIMIT 1")
    Beer getLastBeer();

    @Insert
    void insertAll(Beer... beers);

    @Insert
    void insert(Beer beer);

    @Update
    void update(Beer beer);

    @Delete
    void delete(Beer beer);

    @Query("DELETE FROM beer")
    void deleteAllNotes();

    @Query("SELECT * FROM beer WHERE date >= :date")
    List<Beer> getLastDayBeers(Date date);
}
