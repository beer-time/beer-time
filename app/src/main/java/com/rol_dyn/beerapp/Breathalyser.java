package com.rol_dyn.beerapp;

import androidx.core.math.MathUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Breathalyser {
    static private final float GENDER_CONSTANT_MALE = 0.68f;
    static private final float GENDER_CONSTANT_FEMALE = 0.55f;
    static private final float MLS_TO_GRAMS_CONSTANT = 0.789f;
    static private final float HOUR_CONSTANT = 0.15f;

    public static float getGramsOfAlcoholConsumed(List<Beer> beers) {
        float gramsOfAlcohol = 0f;
        for (Beer beer : beers) {
            gramsOfAlcohol += (beer.getPercents() / 100) * beer.getCapacity() * MLS_TO_GRAMS_CONSTANT;
        }

        return gramsOfAlcohol;
    }

    public static float multiplyWeightByGenderConstant(float weight, boolean isMale) {
        if (isMale) {
            return weight * 1000 * GENDER_CONSTANT_MALE;
        } else {
            return weight * 1000 * GENDER_CONSTANT_FEMALE;
        }
    }

    public static float getRawAlcoholGramsInBody(float multipliedWeight, float consumedAlcoholInGrams) {
        return consumedAlcoholInGrams / multipliedWeight;
    }

    public static float getTotalAlcoholInPerMiles(float rawAlcoholGrams) {
        return rawAlcoholGrams * 1000;
    }

    public static float getAlcoholLevelBasedOnTimeElapsed(float totalAlcoholInPerMiles, float elapsedTimeInHours) {
        return totalAlcoholInPerMiles - (elapsedTimeInHours * HOUR_CONSTANT);
    }

    public static float getBAC(List<Beer> beers, boolean isMale, float weight) {
        if (beers.size() == 0)
            return 0;
        float gramsOfAlcohol = getGramsOfAlcoholConsumed(beers);
        float multipliedWeight = multiplyWeightByGenderConstant(weight, isMale);
        float rawAlcohol = getRawAlcoholGramsInBody(multipliedWeight, gramsOfAlcohol);
        float perMiles = getTotalAlcoholInPerMiles(rawAlcohol);
        return MathUtils.clamp(Math.round(getAlcoholLevelBasedOnTimeElapsed(perMiles, getTimeDifference(beers.get(beers.size() - 1))) * 100.0f) / 100.0f, 0, 15);

    }

    public static float getTimeDifference(Beer beer) {
        Date lastBeerDate = beer.getDate();
        long diff = Calendar.getInstance().getTime().getTime() - lastBeerDate.getTime();
        float hours = (float) diff / (float) (1000 * 60 * 60);
        return hours;
    }
}
