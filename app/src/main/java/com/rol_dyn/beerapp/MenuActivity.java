package com.rol_dyn.beerapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.rol_dyn.beerapp.ui.history.HistoryFragment;
import com.rol_dyn.beerapp.ui.home.HomeFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Calendar;
import java.util.List;

public class MenuActivity extends AppCompatActivity {

    //Shared preferences value
    public float perMilTarget = 0.0f;
    public float weight;
    public boolean isMale;
    private BeerViewModel beerViewModel;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        checkFirstLaunch();
        getSharedPreferencesValues();
        setContentView(R.layout.activity_menu);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, new HomeFragment()).commit();

        beerViewModel = new ViewModelProvider(this).get(BeerViewModel.class);

        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            int previousItem = 1;

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;

                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        selectedFragment = new HomeFragment();
                        break;
                    case R.id.navigation_history:
                        selectedFragment = new HistoryFragment();
                        break;
                    case R.id.navigation_settings:
                        selectedFragment = new com.rol_dyn.beerapp.ui.settings.SettingsFragment();
                        break;
                }
                if(previousItem == item.getOrder()){
                    return true;
                }
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                if (previousItem < item.getOrder()) {
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                } else {
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                }
                fragmentTransaction.replace(R.id.nav_host_fragment, selectedFragment).commit();
                previousItem = item.getOrder();
                return true;
            }
        });
    }

    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utils.allPermissionsGranted(this)) {
            Utils.requestRuntimePermissions(this);
        }
    }

    public List<Beer> getLastDayBeers() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return beerViewModel.getLastDayBeers(cal.getTime());
    }

    public Beer getLastBeer() {
        return beerViewModel.getLastBeer();
    }

    public BeerViewModel getBeerViewModel() {
        return beerViewModel;
    }

    public void getSharedPreferencesValues() {
        sharedPreferences = getSharedPreferences("userDetails", MODE_PRIVATE);
        perMilTarget = sharedPreferences.getFloat("perMilTarget", 1.0f);
        weight = sharedPreferences.getFloat("weight", 50f);
        isMale = sharedPreferences.getBoolean("isMale", true);
    }

    private void checkFirstLaunch() {
        sharedPreferences = getSharedPreferences("userDetails", MODE_PRIVATE);
        boolean firstStart = sharedPreferences.getBoolean("firstStart", true);
        if (firstStart) {
            Intent intent = new Intent(this, SettingsActivity.class);
            finish();
            startActivity(intent);
        }

    }

    public float getPerMilTarget() {
        return perMilTarget;
    }

    public float getWeight() {
        return weight;
    }

    public boolean isMale() {
        return isMale;
    }
}
