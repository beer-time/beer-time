package com.rol_dyn.beerapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.Date;
import java.util.List;

public class BeerAppRepository {
    private BeerDao beerDao;
    private LiveData<List<Beer>> allBeers;

    public BeerAppRepository(Application application) {
        BeerAppDatabase database = BeerAppDatabase.getInstance(application);
        beerDao = database.beerDao();
        allBeers = beerDao.getAll();
    }

    public void insert(Beer beer) {
        new InsertBeerAsyncTask(beerDao).execute(beer);
    }

    public void update(Beer beer) {
        new UpdateBeerAsyncTask(beerDao).execute(beer);
    }

    public void delete(Beer beer) {
        new DeleteBeerAsyncTask(beerDao).execute(beer);
    }

    public void deleteAllBeers() {
        new DeleteAllBeerAsyncTask(beerDao).execute();
    }

    public LiveData<List<Beer>> getAllBeers() {
        return allBeers;
    }

    public Beer getLastBeer() {
        return beerDao.getLastBeer();
    }

    public List<Beer> getLastDayBeers(Date date) {
        return beerDao.getLastDayBeers(date);
    }


    private static class InsertBeerAsyncTask extends AsyncTask<Beer, Void, Void> {
        private BeerDao beerDao;

        private InsertBeerAsyncTask(BeerDao beerDao) {
            this.beerDao = beerDao;
        }

        @Override
        protected Void doInBackground(Beer... beers) {
            beerDao.insert(beers[0]);
            return null;
        }
    }

    private static class UpdateBeerAsyncTask extends AsyncTask<Beer, Void, Void> {
        private BeerDao beerDao;

        private UpdateBeerAsyncTask(BeerDao beerDao) {
            this.beerDao = beerDao;
        }

        @Override
        protected Void doInBackground(Beer... beers) {
            beerDao.update(beers[0]);
            return null;
        }
    }

    private static class DeleteBeerAsyncTask extends AsyncTask<Beer, Void, Void> {
        private BeerDao beerDao;

        private DeleteBeerAsyncTask(BeerDao beerDao) {
            this.beerDao = beerDao;
        }

        @Override
        protected Void doInBackground(Beer... beers) {
            beerDao.delete(beers[0]);
            return null;
        }
    }

    private static class DeleteAllBeerAsyncTask extends AsyncTask<Void, Void, Void> {
        private BeerDao beerDao;

        private DeleteAllBeerAsyncTask(BeerDao beerDao) {
            this.beerDao = beerDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            beerDao.deleteAllNotes();
            return null;
        }
    }


}
