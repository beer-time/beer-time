package com.rol_dyn.beerapp;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.Date;
import java.util.List;

public class BeerViewModel extends AndroidViewModel {
    private BeerAppRepository repository;
    private LiveData<List<Beer>> allBeers;

    public BeerViewModel(@NonNull Application application) {
        super(application);
        repository = new BeerAppRepository(application);
        allBeers = repository.getAllBeers();
    }

    public void insert(Beer beer) {
        repository.insert(beer);
    }

    public void update(Beer beer) {
        repository.update(beer);
    }

    public void delete(Beer beer) {
        repository.delete(beer);
    }

    public void deleteAllBeers() {
        repository.deleteAllBeers();
    }

    public LiveData<List<Beer>> getAllBeers() {
        return allBeers;
    }

    public List<Beer> getLastDayBeers(Date date) {
        return repository.getLastDayBeers(date);
    }

    public Beer getLastBeer() {
        return repository.getLastBeer();
    }
}
