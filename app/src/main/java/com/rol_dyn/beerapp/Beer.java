package com.rol_dyn.beerapp;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity
public class Beer {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "date")
    private Date date;
    @ColumnInfo(name = "percents")
    private float percents;
    @ColumnInfo(name = "capacity")
    private int capacity;
    @ColumnInfo(name = "name")
    private String name;

    @Ignore
    public Beer() {
        // Default constructor required for calls to DataSnapshot.getValue(Beer.class)
    }
    public Beer(float percents, int capacity, String name, Date date) {
        this.name = name;
        this.percents = percents;
        this.capacity = capacity;
        this.date = date;
    }

    @Ignore
    public Beer(Date date, float percents, int capacity) {
        this.date = date;
        this.percents = percents;
        this.capacity = capacity;
    }
    @Ignore
    public Beer(float percents, int capacity, String name) {
        this.name = name;
        this.percents = percents;
        this.capacity = capacity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPercents() {
        return percents;
    }

    public void setPercents(float percents) {
        this.percents = percents;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
