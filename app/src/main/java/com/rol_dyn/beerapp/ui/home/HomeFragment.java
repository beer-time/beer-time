package com.rol_dyn.beerapp.ui.home;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.rol_dyn.beerapp.Beer;
import com.rol_dyn.beerapp.BeerFillable;
import com.rol_dyn.beerapp.Breathalyser;
import com.rol_dyn.beerapp.LiveBarcodeScanningActivity;
import com.rol_dyn.beerapp.MenuActivity;
import com.rol_dyn.beerapp.R;
import com.rol_dyn.beerapp.Utils;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class HomeFragment extends Fragment {

    int SCAN_ACTIVITY = 1;
    private MenuActivity activity;
    private float perMileTarget;
    private View root;
    private MediaPlayer lastBeerPlayer;
    private BeerFillable beerFillable;
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SCAN_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                //Add loading dialog
                View rootL = getLayoutInflater().inflate(R.layout.loading, null);
                AlertDialog loadingAlertDialog = new MaterialAlertDialogBuilder(getActivity())
                        .setView(rootL).create();
                String result = data.getStringExtra("barcode");
                loadingAlertDialog.show();
                if (result != null) {
                    DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("Beer");

                    db.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            loadingAlertDialog.dismiss();
                            if (!dataSnapshot.child(result).exists()) {
                                //Not in Database
                                showWannaAddBeerDialog(result);
                            } else {
                                //In database
                                Beer beerToDb = dataSnapshot.child(result).getValue(Beer.class);
                                showWannaAddFoundedBeer(beerToDb, result);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            loadingAlertDialog.dismiss();
                        }
                    });

                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setActionBarTitle(getResources().getString(R.string.title_home));
    }

    private void addBeerToDb(Beer beer) {
        if (beer == null) {
            Snackbar.make(root, R.string.lastBeerNotFound, Snackbar.LENGTH_LONG)
                    .setAction(R.string.dismiss, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Dismiss
                        }
                    }).show();
        } else {
            activity.getBeerViewModel().insert(beer);
            lastBeerPlayer.seekTo(0);
            lastBeerPlayer.start();
            Snackbar.make(root, R.string.beerAdded, Snackbar.LENGTH_LONG)
                    .setAction(R.string.dismiss, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Dismiss
                        }
                    }).show();
        }
    }

    private void menuButtonsLogic(final View root) {
        ImageButton lastBeerButton = root.findViewById(R.id.btn_lastBeer);
        lastBeerButton.setOnClickListener(v -> {
            Beer beer = activity.getLastBeer();
            if(beer != null) {
                addBeerToDb(new Beer(beer.getPercents(), beer.getCapacity(), beer.getName(), Calendar.getInstance().getTime()));
            }
            else
            {
                addBeerToDb(null);
            }

        });
        ImageButton newBeerButton = root.findViewById(R.id.btn_newBeer);
        newBeerButton.setOnClickListener(v -> {
            showAddBeerToLocalDb();
        });

        ImageButton scanBarcode = root.findViewById(R.id.btn_scanBarcode);
        scanBarcode.setOnClickListener(v -> {
            if (!Utils.allPermissionsGranted(getActivity())) {
                 Utils.requestRuntimePermissions(getActivity());
            }else {
                Intent intent = new Intent(getActivity(), LiveBarcodeScanningActivity.class);
                startActivityForResult(intent, SCAN_ACTIVITY);
            }
        });
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home, container, false);
        activity = (MenuActivity) getActivity();
        if (activity == null) {
            return null;
        }
        activity.getSharedPreferencesValues();
        perMileTarget = activity.perMilTarget;
        beerFillable = root.findViewById(R.id.BeerFillable);
        beerFillable.setMaxPermile(perMileTarget);
        beerFillable.setPerMileFilled(Breathalyser.getBAC(activity.getLastDayBeers(), activity.isMale, activity.weight));
        lastBeerPlayer = MediaPlayer.create(root.getContext(), R.raw.beer_last);
        activity.getBeerViewModel().getAllBeers().observe(getViewLifecycleOwner(), new Observer<List<Beer>>() {
            @Override
            public void onChanged(List<Beer> beers) {
                    BeerFillable beerFillable = root.findViewById(R.id.BeerFillable);
                    beerFillable.setMaxPermile(perMileTarget);
                    beerFillable.setPerMileFilled(Breathalyser.getBAC(activity.getLastDayBeers(), activity.isMale, activity.weight));
                    beerFillable.redraw();
            }
        });

        menuButtonsLogic(root);

        return root;
    }

    private void showWannaAddFoundedBeer(Beer beer, String barcode) {
        final View rootFounded = getLayoutInflater().inflate(R.layout.beer_summary, null);
        ((TextView) rootFounded.findViewById(R.id.tv_name)).setText(beer.getName());
        ((TextView) rootFounded.findViewById(R.id.tv_capacity)).setText(String.format("%d", beer.getCapacity()));
        ((TextView) rootFounded.findViewById(R.id.tv_percents)).setText(String.format("%2.1f", beer.getPercents()));
        new MaterialAlertDialogBuilder(getActivity())
                .setTitle(R.string.beerFound)
                .setView(rootFounded)
                .setMessage(R.string.wannaAddDrinkedBeer)
                .setNegativeButton(R.string.cancel, null)
                .setNeutralButton(R.string.edit, (dialog, which) -> {
                    showEditBeerDialog(beer, barcode);
                })
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    addBeerToDb(new Beer(beer.getPercents(), beer.getCapacity(), beer.getName(), Calendar.getInstance().getTime()));
                })
                .show();


    }

    private void showEditBeerDialog(Beer beer, String barcode) {
        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("Beer");
        final View rootFB = getLayoutInflater().inflate(R.layout.add_beer_firebase, null);

        TextInputLayout percents = rootFB.findViewById(R.id.percents_firebase_TF);
        TextInputLayout name = rootFB.findViewById(R.id.name_firebase_TF);
        TextInputLayout capacity = rootFB.findViewById(R.id.capacity_firebase_TF);

        MaterialAlertDialogBuilder dialog = new MaterialAlertDialogBuilder(getActivity());
        dialog.setTitle(R.string.dialog_title_add_beer_to_database);
        dialog.setMessage(R.string.newBeerDescription);
        dialog.setView(rootFB);
        dialog.setPositiveButton(R.string.add, null);
        dialog.setNegativeButton(R.string.cancel, (dialog1, which) -> {
            dialog1.dismiss();
        });
        AlertDialog alertDialog = dialog.create();
        alertDialog.setOnShowListener(dialog1 -> {
            Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(v -> {
                if (validateAddBeerInputs(rootFB, percents, name, capacity)) {
                    float fPercents = Float.parseFloat(percents.getEditText().getText().toString());
                    int iCapacity = Integer.parseInt(capacity.getEditText().getText().toString());
                    db.child(barcode).setValue(new Beer(fPercents, iCapacity, name.getEditText().getText().toString().toUpperCase()));
                    addBeerToDb(new Beer(fPercents, iCapacity, name.getEditText().getText().toString(), Calendar.getInstance().getTime()));
                    dialog1.dismiss();
                }
            });
        });
        alertDialog.show();

        ((TextInputLayout) rootFB.findViewById(R.id.percents_firebase_TF)).getEditText().setText(String.format(Locale.US, "%.1f", beer.getPercents()));
        ((TextInputLayout) rootFB.findViewById(R.id.name_firebase_TF)).getEditText().setText(beer.getName());
        ((TextInputLayout) rootFB.findViewById(R.id.capacity_firebase_TF)).getEditText().setText(String.format("%d", beer.getCapacity()));
    }

    private void showWannaAddBeerDialog(String barcode) {
        new MaterialAlertDialogBuilder(getActivity())
                .setTitle(R.string.beerNotFound)
                .setMessage(R.string.wannaAddBeer)
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    addBeerToFirebase(barcode);
                })
                .show();
    }

    private void addBeerToFirebase(String barCode) {
        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("Beer");
        final View rootFB = getLayoutInflater().inflate(R.layout.add_beer_firebase, null);

        TextInputLayout percents = rootFB.findViewById(R.id.percents_firebase_TF);
        TextInputLayout name = rootFB.findViewById(R.id.name_firebase_TF);
        TextInputLayout capacity = rootFB.findViewById(R.id.capacity_firebase_TF);
        MaterialAlertDialogBuilder dialog = new MaterialAlertDialogBuilder(getActivity());
        dialog.setTitle(R.string.dialog_title_add_beer_to_database);
        dialog.setMessage(R.string.newBeerDescription);
        dialog.setView(rootFB);
        dialog.setPositiveButton(R.string.add, null);
        dialog.setNegativeButton(R.string.cancel, (dialog1, which) -> {
            dialog1.dismiss();
        });
        AlertDialog alertDialog = dialog.create();
        alertDialog.setOnShowListener(dialog1 -> {
            Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(v -> {
                if (validateAddBeerInputs(rootFB, percents, name, capacity)) {
                    float fPercents = Float.parseFloat(percents.getEditText().getText().toString());
                    int iCapacity = Integer.parseInt(capacity.getEditText().getText().toString());
                    Log.d("ADD BEER TO FIREBASE", "addBeerToFirebase: " + fPercents  );
                    db.child(barCode).setValue(new Beer(fPercents, iCapacity, name.getEditText().getText().toString().toUpperCase()));
                    addBeerToDb(new Beer(fPercents, iCapacity, name.getEditText().getText().toString(), Calendar.getInstance().getTime()));
                    dialog1.dismiss();
                }
            });
        });
        alertDialog.show();
    }

    private void showAddBeerToLocalDb() {
        final View rootLDb = getLayoutInflater().inflate(R.layout.add_beer_layout, null);
        TextInputLayout percents = rootLDb.findViewById(R.id.tiPercents);
        TextInputLayout name = rootLDb.findViewById(R.id.tiName);
        TextInputLayout capacity = rootLDb.findViewById(R.id.tiCapacity);
        MaterialAlertDialogBuilder dialog = new MaterialAlertDialogBuilder(getActivity());
        dialog.setTitle(R.string.addNew);
        dialog.setMessage(R.string.newBeerDescription);
        dialog.setView(rootLDb);
        dialog.setPositiveButton(R.string.add, null);
        dialog.setNegativeButton(R.string.cancel, (dialog1, which) -> {
            dialog1.dismiss();
        });
        AlertDialog alertDialog = dialog.create();
        alertDialog.setOnShowListener(dialog1 -> {
            Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(v -> {
                if (validateAddBeerInputs(rootLDb, percents, name, capacity)) {
                    float fPercents = Float.parseFloat(percents.getEditText().getText().toString());
                    int iCapacity = Integer.parseInt(capacity.getEditText().getText().toString());
                    addBeerToDb(new Beer(fPercents, iCapacity, name.getEditText().getText().toString(), Calendar.getInstance().getTime()));
                    dialog1.dismiss();
                }
            });
        });
        alertDialog.show();

    }

    private boolean validateAddBeerInputs(View root, TextInputLayout percents, TextInputLayout name, TextInputLayout capacity) {
        boolean correct = true;

        if (percents.getEditText().getText().toString().equals("")) {
            percents.setError(getResources().getString(R.string.percentsAreEmpty));
            correct = false;
        }
        if (name.getEditText().getText().toString().equals("")) {
            name.setError(getResources().getString(R.string.nameIsEmpty));
            correct = false;
        }
        if (capacity.getEditText().getText().toString().equals("")) {
            capacity.setError(getResources().getString(R.string.capacityAreEmpty));
            correct = false;
        }

        return correct;
    }

}