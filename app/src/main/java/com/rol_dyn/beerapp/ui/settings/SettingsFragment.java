package com.rol_dyn.beerapp.ui.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.rol_dyn.beerapp.MenuActivity;
import com.rol_dyn.beerapp.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import static android.content.Context.MODE_PRIVATE;

public class SettingsFragment extends Fragment {
private MenuActivity menuActivity;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_settings, container, false);
         menuActivity = (MenuActivity) getActivity();
        TextView genderTW = root.findViewById(R.id.gender_text_view);
        TextView weightTw = root.findViewById(R.id.weight_text_view);
        TextView goalTW = root.findViewById(R.id.goal_text_view);
        weightTw.setText(String.valueOf(menuActivity.getWeight()));
        goalTW.setText(String.valueOf(menuActivity.getPerMilTarget()));
        if (menuActivity.isMale) {
            genderTW.setText(getResources().getText(R.string.male_text));
        } else {
            genderTW.setText(getResources().getText(R.string.female_text));
        }


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("userDetails", MODE_PRIVATE);
        Button goalButton = root.findViewById(R.id.edit_goal_btn);
        Button genderButton = root.findViewById(R.id.edit_gender_btn);
        Button weightButton = root.findViewById(R.id.edit_weight_btn);

        editGoal(goalTW, sharedPreferences, goalButton);

        editGender(genderTW, sharedPreferences, genderButton);

        editWeight(weightTw, sharedPreferences, weightButton);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        menuActivity.setActionBarTitle(getResources().getString(R.string.title_settings));
    }

    private void editGoal(TextView goalTW, SharedPreferences sharedPreferences, Button goalButton) {
        goalButton.setOnClickListener(v -> {
            EditText editText = new EditText(getActivity());
            editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            new MaterialAlertDialogBuilder(getActivity())
                    .setTitle(R.string.edit_goal_text)
                    .setView(editText)
                    .setPositiveButton(R.string.ok, (dialog, which) -> {
                        if (!editText.getText().toString().matches("")) {
                            String goal = editText.getText().toString();
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putFloat("perMilTarget", Float.parseFloat(goal));
                            editor.apply();
                            goalTW.setText(goal);
                            ((MenuActivity) getActivity()).getSharedPreferencesValues();
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();

        });
    }

    private void editGender(TextView genderTW, SharedPreferences sharedPreferences, Button genderButton) {
        genderButton.setOnClickListener(v -> {
            RadioGroup radioGroup = new RadioGroup(getActivity());
            radioGroup.setOrientation(RadioGroup.VERTICAL);
            RadioButton maleRB = new RadioButton(getActivity());
            maleRB.setText(getResources().getString(R.string.male_text));
            RadioButton femaleRB = new RadioButton(getActivity());
            femaleRB.setText(getResources().getString(R.string.female_text));
            radioGroup.addView(maleRB);
            radioGroup.addView(femaleRB);
            new MaterialAlertDialogBuilder(getActivity())
                    .setTitle(R.string.edit_gender_text)
                    .setView(radioGroup)
                    .setPositiveButton(R.string.ok, (dialog, which) -> {
                        if (maleRB.isChecked()) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean("isMale", true);
                            editor.apply();
                            genderTW.setText(maleRB.getText());
                            ((MenuActivity) getActivity()).getSharedPreferencesValues();
                        }
                        if (femaleRB.isChecked()) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean("isMale", false);
                            editor.apply();
                            genderTW.setText(femaleRB.getText());
                            ((MenuActivity) getActivity()).getSharedPreferencesValues();
                        }
                    })
                    .show();
        });
    }

    private void editWeight(TextView weightTw, SharedPreferences sharedPreferences, Button weightButton) {
        weightButton.setOnClickListener(v -> {
            EditText editText = new EditText(getActivity());
            editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

            new MaterialAlertDialogBuilder(getActivity())
                    .setTitle(R.string.edit_weight_text)
                    .setView(editText)
                    .setPositiveButton(R.string.ok, (dialog, which) -> {
                        if (!editText.getText().toString().matches("")) {
                            String goal = editText.getText().toString();
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putFloat("weight", Float.parseFloat(goal));
                            editor.apply();
                            weightTw.setText(goal);
                            ((MenuActivity) getActivity()).getSharedPreferencesValues();
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();

        });
    }


}