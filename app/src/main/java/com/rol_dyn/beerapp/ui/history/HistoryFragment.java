package com.rol_dyn.beerapp.ui.history;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rol_dyn.beerapp.Beer;
import com.rol_dyn.beerapp.BeerAdapter;
import com.rol_dyn.beerapp.BeerViewModel;
import com.rol_dyn.beerapp.MenuActivity;
import com.rol_dyn.beerapp.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;


public class HistoryFragment extends Fragment {
    private BeerViewModel beerViewModel;
    private BeerAdapter beerAdapter;
    private MenuActivity activity;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_history, container, false);
        RecyclerView recyclerView = root.findViewById(R.id.recycler_view_history);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        recyclerView.setHasFixedSize(true);
        activity = (MenuActivity)getActivity();
        beerAdapter = new BeerAdapter();
        recyclerView.setAdapter(beerAdapter);
        beerViewModel = ((MenuActivity) getActivity()).getBeerViewModel();
        beerViewModel.getAllBeers().observe(getViewLifecycleOwner(), beers -> beerAdapter.setBeers(beers));
        setHasOptionsMenu(true);

        return root;
    }
    @Override
    public void onResume() {
        super.onResume();
        activity.setActionBarTitle(getResources().getString(R.string.title_history));
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.history_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();


        if (itemId == R.id.action_delete) {
            Log.d("DELETED_BUTTON", " Deleted button clicked");

            new MaterialAlertDialogBuilder(getActivity())
                    .setTitle(getResources().getString(R.string.confirm_dialog_title))
                    .setMessage(getResources().getString(R.string.confirm_dialog_content))
                    .setNegativeButton(getResources().getString(R.string.no), null)
                    .setPositiveButton(getResources().getString(R.string.yes), (dialogInterface, i) -> {
                        beerViewModel.deleteAllBeers();
                        showSnackBar(getView(), getResources().getString(R.string.history_deleted));

                    })
                    .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSnackBar(View root, String message) {
        Snackbar.make(root, message, Snackbar.LENGTH_LONG)
                .setAction(R.string.dismiss, v -> {
                    //Dismiss
                }).show();
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == 2) {
            Log.d("TAG", "onContextItemSelected: DELETED");
            int beerPosition = item.getGroupId();
            Beer toDelete = beerAdapter.getItemAt(beerPosition);
            beerAdapter.removeItemAt(beerPosition);
            beerViewModel.delete(toDelete);
            showSnackBar(getView(), getResources().getString(R.string.beer_deleted_text));
            return true;
        }
        if (item.getItemId() == 1) {
            Log.d("TAG", "onContextItemSelected: EDIT");
            int beerPosition = item.getGroupId();
            showEditBeerDialog(beerPosition);

            return true;
        }
        return super.onContextItemSelected(item);
    }

    private void showEditBeerDialog(int position) {
        Beer oldBeer = beerAdapter.getItemAt(position);

        final View rootLDb = getLayoutInflater().inflate(R.layout.add_beer_layout, null);
        TextInputLayout percents = rootLDb.findViewById(R.id.tiPercents);
        TextInputLayout name = rootLDb.findViewById(R.id.tiName);
        TextInputLayout capacity = rootLDb.findViewById(R.id.tiCapacity);

        percents.getEditText().setText(String.valueOf(oldBeer.getPercents()));
        name.getEditText().setText(String.valueOf(oldBeer.getName()));
        capacity.getEditText().setText(String.valueOf(oldBeer.getCapacity()));

        MaterialAlertDialogBuilder dialog = new MaterialAlertDialogBuilder(getActivity());
        dialog.setTitle(R.string.edit_text);
        dialog.setView(rootLDb);
        dialog.setPositiveButton(R.string.edit, null);
        dialog.setNegativeButton(R.string.cancel, (dialog1, which) -> {
            dialog1.dismiss();
        });
        AlertDialog alertDialog = dialog.create();
        alertDialog.setOnShowListener(dialog1 -> {
            Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(v -> {
                if (validateAddBeerInputs(rootLDb, percents, name, capacity)) {
                    float fPercents = Float.parseFloat(percents.getEditText().getText().toString());
                    int iCapacity = Integer.parseInt(capacity.getEditText().getText().toString());
                    String beerName = name.getEditText().getText().toString();


                    Beer newBeer = new Beer(fPercents, iCapacity, beerName, oldBeer.getDate());
                    newBeer.setId(oldBeer.getId());
                    beerAdapter.updateItemAt(position, newBeer);
                    beerViewModel.update(newBeer);
                    dialog1.dismiss();
                    showSnackBar(getView(), getResources().getString(R.string.beer_updated_text));
                }
            });
        });
        alertDialog.show();

    }

    private boolean validateAddBeerInputs(View root, TextInputLayout percents, TextInputLayout name, TextInputLayout capacity) {
        boolean correct = true;

        if (percents.getEditText().getText().toString().equals("")) {
            percents.setError(getResources().getString(R.string.percentsAreEmpty));
            correct = false;
        }
        if (name.getEditText().getText().toString().equals("")) {
            name.setError(getResources().getString(R.string.nameIsEmpty));
            correct = false;
        }
        if (capacity.getEditText().getText().toString().equals("")) {
            capacity.setError(getResources().getString(R.string.capacityAreEmpty));
            correct = false;
        }

        return correct;
    }
}
