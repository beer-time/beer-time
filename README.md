# Beer App

## [Google Play Store](https://play.google.com/store/apps/details?id=com.rol_dyn.beerapp)

## Application monitoring BAC (Blood Alcohol Content).
<img src="/uploads/4a39a3e34838dc8ae0886b9ed6a9620e/Screenshot_2020-05-18-11-05-02-228_com.example.beerapp.jpg" width="300" />
<img src="/uploads/6fb282115688016a983431cc356e2eeb/Screenshot_2020-05-17-18-12-24-255_com.example.beerapp.jpg" width="300" />

## Features 
* adding last beer last beer from history
* adding beer manually
* animated glass of beer that shows your BAC
* adding beer by scanning barcode using ML Kit
* barcodes with beer data are stored in remote database (firebase)
* history of drinked beers are stored in local database (ROOM, SQLite)
* clearing all history
* remove/edit single record from history
* preview/edit your personal settings
* user friendly interface 
* support dark and light themes
* english/polish language


## Screenshots
<img src="/uploads/ddd8f24bcbd49b1490d68925a4407faf/97679795_604569136813873_4637760339679641600_n.png" width="300" />
<img src="/uploads/b6f52c77cd37e8f3502df7f1d208be33/98204219_244241333681346_6043814112767508480_n.png" width="300" />
<img src="/uploads/dd4fa066b9526d9d79237da4a70f9982/Screenshot_2020-05-16-13-36-43-266_com.example.beerapp.jpg" width="300" />
<img src="/uploads/ddd8a77a30558e2167ff25f76ca97e69/Screenshot_2020-05-16-12-26-39-968_com.example.beerapp.jpg" width="300" />
<img src="/uploads/65b27beef5ba5d1d55a85f410a28c3f8/Screenshot_2020-05-16-12-26-32-452_com.example.beerapp.jpg" width="300" />
<img src="/uploads/0aecb8eba2071f5e0b3e3bd59a97bb60/Screenshot_2020-05-18-11-05-02-228_com.example.beerapp.jpg" width="300" />
<img src="/uploads/555d40d85307afe3934a942b0a6bcd8b/Screenshot_2020-05-18-10-59-35-060_com.example.beerapp.jpg" width="300" />
<img src="/uploads/e80b0680dacf60427130dd6e776ac3db/Screenshot_2020-05-18-10-59-08-066_com.example.beerapp.jpg" width="300" />
<img src="/uploads/00cce377c5a284fd9c1c7b1dac3f0a3f/Screenshot_2020-05-18-10-58-30-588_com.example.beerapp.jpg" width="300" />
<img src="/uploads/80db284d4a87f671aec78a9d049fd878/Screenshot_2020-05-18-10-56-57-209_com.example.beerapp.jpg" width="300" />
<img src="/uploads/a8afd7e04f63c9350cdf80c8caa0879e/Screenshot_2020-05-18-10-56-20-901_com.example.beerapp.jpg" width="300" />
